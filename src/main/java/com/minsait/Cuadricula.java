package com.minsait;

import java.util.ArrayList;
import java.util.List;

public class Cuadricula {

    private final int tamanio;
    private final List<List<Casilla>> casillas = new ArrayList<List<Casilla>>();

    public int getTamanio() {
        return tamanio;
    }

    public Cuadricula(int tamano) {
        this.tamanio = tamano;
        inicializarTablero();
    }

    public List<List<Casilla>> getCasillas() {
        return casillas;
    }

    private void inicializarTablero() {
        for (int indiceRenglon = 0; indiceRenglon < tamanio; indiceRenglon++) {
            casillas.add(new ArrayList<>());
            for (int indiceColumna = 0; indiceColumna < tamanio; indiceColumna++) {
                casillas.get(indiceRenglon).add(new Casilla(indiceRenglon, indiceColumna,
                        Color.reset, ' '));
            }
        }
    }

    public void limpiarCuadricula(){
        for (List<Casilla> renglon: casillas) {
            for (Casilla casilla:
                 renglon) {
                casilla.setLetra(' ');
                casilla.setColor(Color.reset);
            }
        }
    }

    public void imprimirCuadricula(){
        for (List<Casilla> renglon:
                casillas) {
            for (Casilla casilla:
                    renglon) {
                System.out.printf(casilla.getColor().getValue() + "%4s" + Color.reset.getValue(),casilla.getLetra());
            }
            System.out.println("\n");
        }
    }

}
