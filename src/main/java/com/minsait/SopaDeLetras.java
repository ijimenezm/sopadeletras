package com.minsait;

import sun.security.krb5.internal.PAData;

import java.util.*;

public class SopaDeLetras {
    private Cuadricula cuadricula;
    private final HashMap<String,List<Casilla>> mapPalabrasPosiciones;
    private final List<String> listaPalabras;
    private final Dificultad dificultad;
    private final Random random;
    private static final char[] abcedario = {'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r',
            's','t','u','v','w','x','y','z'};
    private final String[] listaPalabrasDeEntrada;

    public SopaDeLetras(int tamanioCuadricula, Dificultad dificultad, int numPalabras, String[] listaDePalabrasDeEntrada){
        this.cuadricula = new Cuadricula(tamanioCuadricula);
        this.mapPalabrasPosiciones = new HashMap<>();
        this.random = new Random();
        this.dificultad = dificultad;
        this.listaPalabrasDeEntrada = listaDePalabrasDeEntrada;
        Palabras palabras = new Palabras();
        this.listaPalabras = palabras.getPalabrasAleatoriasOrdenadas(
                numPalabras - listaDePalabrasDeEntrada.length
        );
    }

    public void crearSopa(){
        Palabras palabras = new Palabras();
        listaPalabras.addAll(Arrays.asList(listaPalabrasDeEntrada));
        for (String palabra: listaPalabras) {
            colocarPalabraEnPosicionAleatoria(palabra);
        }
        llenarLetrasRandom();
        cuadricula.imprimirCuadricula();
        cuadricula.limpiarCuadricula();
    }

    public void colocarPalabraEnPosicionAleatoria(String palabra){
        List<Casilla> casillas = new ArrayList<>();
        if(cuadricula.getTamanio() >= palabra.length()){
            casillas = intentarColocarPalabra(palabra, casillas);
        }else {
            casillas = null;
        }
        mapPalabrasPosiciones.put(palabra,casillas);
    }

    private List<Casilla> intentarColocarPalabra(String palabra, List<Casilla> casillas) {
        int alineacion;
        int dificultad;
        if (this.dificultad == Dificultad.FACIL){
            dificultad = 2;
        }else {
            dificultad = 4;
        }
        for (int intento = 0; intento < 10; intento++) {
            alineacion = random.nextInt(dificultad);
            switch (alineacion){
                case 0:
                    casillas =  colocarPalabraHorizontal(palabra);
                    break;
                case 1:
                    casillas = colocarPalabraVertical(palabra);
                    break;
                case 2:
                    casillas = colocarPalabraDiagonalPositiva(palabra);
                    break;
                case 3:
                    casillas = colocarPalabraDiagonalNegativa(palabra);
                    break;
            }
            if(casillas != null){
                return casillas;
            }
        }
        return null;
    }

    private List<Casilla> colocarPalabraHorizontal(String palabra) {
        int filaInicial = random.nextInt(cuadricula.getTamanio());
        int columnaInicial = random.nextInt(cuadricula.getTamanio() - palabra.length() + 1);
        palabra = voltearPalabraAleatoriamente(palabra);
        List<Casilla> casillas = new ArrayList<Casilla>();
        for (int i = 0; i < palabra.length(); i++) {
            Casilla casilla = cuadricula.getCasillas().get(filaInicial).get(columnaInicial + i);
            casillas.add(casilla);
        }
        casillas = colocarPalabraSiCasillasEstanVacias(palabra, casillas);
        return casillas;
    }

    private List<Casilla> colocarPalabraVertical(String palabra) {
        int filaInicial = random.nextInt(cuadricula.getTamanio() - palabra.length() + 1);
        int columnaInicial = random.nextInt(cuadricula.getTamanio());
        palabra = voltearPalabraAleatoriamente(palabra);
        List<Casilla> casillas = new ArrayList<Casilla>();
        for (int i = 0; i < palabra.length(); i++) {
            Casilla casilla = cuadricula.getCasillas().get(filaInicial + i).get(columnaInicial);
            casillas.add(casilla);
        }
        casillas = colocarPalabraSiCasillasEstanVacias(palabra, casillas);
        return casillas;
    }

    private List<Casilla> colocarPalabraDiagonalPositiva(String palabra) {
        int filaInicial = palabra.length() - 1 + random.nextInt(cuadricula.getTamanio() - palabra.length() + 1);
        int columnaInicial = random.nextInt(cuadricula.getTamanio() - palabra.length() + 1);
        List<Casilla> casillas = new ArrayList<>();
        palabra = voltearPalabraAleatoriamente(palabra);
        for (int i = 0; i < palabra.length(); i++) {
            Casilla casilla = cuadricula.getCasillas().get(filaInicial - i).get(columnaInicial);
            casillas.add(casilla);
        }
        casillas = colocarPalabraSiCasillasEstanVacias(palabra, casillas);
        return casillas;
    }

    private List<Casilla> colocarPalabraDiagonalNegativa(String palabra) {
        List<Casilla> casillas = new ArrayList<>();
        int filaInicial = random.nextInt(cuadricula.getTamanio() - palabra.length() + 1);
        int columnaInicial = random.nextInt(cuadricula.getTamanio() - palabra.length() + 1);
        palabra = voltearPalabraAleatoriamente(palabra);
        for (int i = 0; i < palabra.length(); i++) {
            Casilla casilla =  cuadricula.getCasillas().get(filaInicial + i).get(columnaInicial + i);
            casillas.add(casilla);
        }
        casillas = colocarPalabraSiCasillasEstanVacias(palabra, casillas);
        return casillas;
    }

    private String voltearPalabraAleatoriamente(String palabra) {
        boolean voltear = random.nextInt(2) == 1;
        if (voltear && dificultad == Dificultad.DIFICIL) {
            palabra = voltearPalabra(palabra);
        }
        return palabra;
    }

    private String voltearPalabra(String palabra){
        StringBuilder sb = new StringBuilder();
        for (int indice = palabra.length(); indice > 0; indice--) {
            sb.append(palabra, indice - 1, indice);
        }
        return sb.toString();
    }

    private List<Casilla> colocarPalabraSiCasillasEstanVacias(String palabra, List<Casilla> casillas) {
        if (checarCasillasVacias(casillas, palabra)) {
            colocarPalabra(palabra, casillas);
        } else {
            casillas = null;
        }
        return casillas;
    }

    private boolean checarCasillasVacias(List<Casilla> casillas, String palabra){
        for (int i = 0; i < casillas.size(); i++) {
            Casilla casilla = casillas.get(i);
            if (casilla.getLetra() != ' ' && casilla.getLetra() != palabra.charAt(i)){
                return false;
            }
        }
        return true;
    }

    private void colocarPalabra(String palabra, List<Casilla> casillas) {
        Color colorAleatorio = Color.values()[1 + random.nextInt(Color.values().length - 1)];
        for (int i = 0; i < palabra.length(); i++) {
            char charActual = palabra.charAt(i);
            Casilla casilla = casillas.get(i);
            casilla.setLetra(charActual);
            casilla.setColor(colorAleatorio);
        }
    }

    private void llenarLetrasRandom(){
        char siguienteLetra;
        for (List<Casilla> renglon:
             cuadricula.getCasillas()) {
            for (Casilla casilla:
                 renglon) {
                siguienteLetra = abcedario[random.nextInt(abcedario.length)];
                if (casilla.getLetra() == ' '){
                    casilla.setLetra(siguienteLetra);
                }
            }
        }
    }

    public void setCuadricula(int tamanioCuadricula){
        cuadricula = new Cuadricula(tamanioCuadricula);
    }

    public List<String> getListaPalabras() {
        return listaPalabras;
    }

    public static void main(String[] args) {
        String[] palabrasDeEntrada = {"corazones","moran","misma"};
        SopaDeLetras sopaDeLetras = new SopaDeLetras(10, Dificultad.DIFICIL,20, palabrasDeEntrada);
        sopaDeLetras.crearSopa();
    }
}
