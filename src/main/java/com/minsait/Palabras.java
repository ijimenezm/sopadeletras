package com.minsait;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.Normalizer;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Palabras {

    private List<String> listaDePalabras;
    Random rand;

    public Palabras() {
        rand = new Random();
        String filePath = "C:/Users/w10/IdeaProjects/sopadeletras/src/main/resources/diccionario.txt";
        try (Stream<String> stream = Files.lines(Paths.get(filePath))) {
            listaDePalabras = stream
                    .map(s-> {
                        s = quitarCaracteresEspeciales(s.toLowerCase());
                        return s;
                    })
                    .collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String quitarCaracteresEspeciales(String texto) {
        texto = Normalizer.normalize(texto, Normalizer.Form.NFD);
        texto = texto.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "");
        return texto;
    }

    public void ordenarListaPalabrasPorTamano(){
        Collections.sort(listaDePalabras, (o1, o2) -> o2.length() - o1.length());
    }

    public List<String> ordenarListaPalabrasPorTamano(List<String> listaDePalabras){
        Collections.sort(listaDePalabras, (o1, o2) -> o2.length() - o1.length());
        return listaDePalabras;
    }

    public void imprimeListaDePalabras(){
        listaDePalabras.forEach(System.out::println);
    }

    public List<String> getPalabrasAleatoriasOrdenadas(int numeroDePalabras){
        int randomIndex;
        String elementoRandom;
        List<String> nuevaListaDePalabras = new ArrayList<>();
        for (int i = 0; i < numeroDePalabras; i++) {
            randomIndex = rand.nextInt(listaDePalabras.size());
            elementoRandom = listaDePalabras.get(randomIndex);
            listaDePalabras.remove(randomIndex);
            nuevaListaDePalabras.add(elementoRandom);
        }
        return ordenarListaPalabrasPorTamano(nuevaListaDePalabras);
    }
}