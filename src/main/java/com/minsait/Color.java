 package com.minsait;

public enum Color {
        reset("\u001B[0m"),
        red("\u001B[41m"),
        green("\u001B[42m"),
        yellow("\u001B[43m"),
        blue("\u001B[44m"),
        magenta("\u001B[45m"),
        cyan("\u001B[46m");
        private final String value;

        Color(String value) {
            this.value = value;
        }
        
        public String getValue() {
            return value;
        }
}