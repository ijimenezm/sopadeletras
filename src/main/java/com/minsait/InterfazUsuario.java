package com.minsait;

import java.util.Scanner;

public class InterfazUsuario {
    int tamanioCuadricula;
    int numPalabras;
    String[] listaDePalabrasDeEntrada;

    public void crearSopa(int tamanioCuadricula, Dificultad dificultad, int numPalabras, String[] listaDePalabrasDeEntrada){
        SopaDeLetras sopaDeLetras = new SopaDeLetras(tamanioCuadricula,dificultad,numPalabras,listaDePalabrasDeEntrada);
        sopaDeLetras.crearSopa();
    }
    public void crearSopa(Dificultad dificultad, int numPalabras, String[] listaDePalabrasDeEntrada){
        SopaDeLetras sopaDeLetras = new SopaDeLetras(0,dificultad,numPalabras,listaDePalabrasDeEntrada);
        int tamanioCuadricula = sopaDeLetras.getListaPalabras().get(0).length();
        sopaDeLetras.setCuadricula(tamanioCuadricula);
        sopaDeLetras.crearSopa();
    }

    public int leerDificultadEntradaEstandar(){
        System.out.println("Ingresa la dificultad de la sopa de letras: " +
                "\n1 : Facil" +
                "\n2 : Medio" +
                "\n3 : Dificil");

        return leerEntradaEstandar("[123]");
    }
    
    public void leerListaPalabrasEntradEstandar(){
        System.out.println("Ingrese la lista de palabras que apareceran en la sopa de letras, deben estar en minusculas" +
                " y separadas por coma" +
                "\n Ejemplo: " +
                "\n\t corazones, moran, misma");
        listaDePalabrasDeEntrada = leerListaEntradaEstandar();
    }

    public void leerNumPalabrasEntradaEstandar(){
        System.out.println("Ingrese el numero de palabras que apareceran en la sopa de letras, debe ser un numero mayor " +
                "a " + listaDePalabrasDeEntrada.length);
        numPalabras = leerEntradaEstandar("[0-9]+");
    }

    public void leerTamanioCuadriculaEntradaEstandar(){
        System.out.println("Ingrese el tamanio de la cuadricula de la sopa de letras, ingrese 0 para que se genere " +
                " una cuadricula de tamanio aleatorio");
        tamanioCuadricula = leerEntradaEstandar("[0-9]+");
    }

    private String[] leerListaEntradaEstandar() {
        Scanner scanner = new Scanner(System.in);
        String entrada = scanner.nextLine();
        if (entrada.matches("[a-z,\\s]+")){
            return entrada.replaceAll(" ","").split(",");
        }
        else {
            System.out.println(entrada + " esta entrada no es correcta");
            return leerListaEntradaEstandar();
        }
    }

    private int leerEntradaEstandar(String regex) {
        Scanner scanner = new Scanner(System.in);
        String entrada = scanner.next();
        if (entrada.matches(regex)){
            return Integer.parseInt(entrada);
        }
        else {
            System.out.println(entrada + " no es una opción, por favor escriba un numero");
            return leerEntradaEstandar(regex);
        }
    }
    
    public void inciar(){
        int entradaDificultad = leerDificultadEntradaEstandar();
        Dificultad dificultad;
        switch (entradaDificultad){
            case 1 :
                dificultad = Dificultad.FACIL;
                break;
            case 2:
                dificultad = Dificultad.MEDIO;
                break;
            case 3 : 
                dificultad = Dificultad.DIFICIL;
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + entradaDificultad);
        }
        leerListaPalabrasEntradEstandar();
        leerNumPalabrasEntradaEstandar();
        leerTamanioCuadriculaEntradaEstandar();
        if (tamanioCuadricula == 0){
            crearSopa(dificultad,numPalabras,listaDePalabrasDeEntrada);
        }
        else {
            crearSopa(tamanioCuadricula,dificultad,numPalabras,listaDePalabrasDeEntrada);
        }
        
    }

    public static void main(String[] args) {
        InterfazUsuario interfazUsuario = new InterfazUsuario();
        interfazUsuario.inciar();
    }
}
