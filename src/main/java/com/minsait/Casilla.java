package com.minsait;

public class Casilla {

    private Integer posicionColumna;
    private Integer posicionFila;
    private Color color;
    private char letra;

    public Casilla(Integer posicionColumna, Integer posicionFila, Color color, char letra) {
        this.posicionColumna = posicionColumna;
        this.posicionFila = posicionFila;
        this.color = color;
        this.letra = letra;
    }

    public Integer getPosicionColumna() {
        return posicionColumna;
    }

    public void setPosicionColumna(Integer posicionColumna) {
        this.posicionColumna = posicionColumna;
    }

    public Integer getPosicionFila() {
        return posicionFila;
    }

    public void setPosicionFila(Integer posicionFila) {
        this.posicionFila = posicionFila;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public char getLetra() {
        return letra;
    }

    public void setLetra(char letra) {
        this.letra = letra;
    }

    @Override
    public String toString() {
        return "Casilla{" +
                "posicionColumna=" + posicionColumna +
                ", posicionFila=" + posicionFila +
                '}';
    }
}

